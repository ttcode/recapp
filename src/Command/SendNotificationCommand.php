<?php

namespace App\Command;

use App\Entity\Notification;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Entity\User;
use App\EntityNotification;
use App\Service\UserService;
use App\Service\NotificationService;

class SendNotificationCommand extends Command
{
    protected static $defaultName = 'send-notification';
    private $userService;
    private $notificationService;

    public function __construct(UserService $userService, NotificationService $notificationService)
    {
        $this->userService = $userService;
        $this->notificationService = $notificationService;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Create and send message to user by contact channels');
        $this->setDefinition(
            new InputDefinition([
                new InputOption('message', 'm', InputOption::VALUE_REQUIRED),
                new InputOption('user', 'u', InputOption::VALUE_REQUIRED)
            ])
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $message = $input->getOption('message');
        $userLogin = $input->getOption('user');
        $userData = $this->userService->getByLogin($userLogin);
        $user = new User($userData->login, $userData->email, $userData->contactChannels);
        $notification = new Notification($message);
        $this->notificationService->send($notification, $user);
        return Command::SUCCESS;
    }
}