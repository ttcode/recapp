<?php

namespace App\Service;

use App\Entity\User;
use App\Interfaces\Users;
use App\Service\JsonFileManagerService;

final class UserService implements Users
{
    private $jsonFileManager;

    public function __construct(JsonFileManagerService $jsonFileManager)
    {
        $this->jsonFileManager = $jsonFileManager;
    }

    public function add(User $user)
    {
        $this->jsonFileManager->putContent($user);
    }

    public function getByEmail(string $email)
    {
        return $this->jsonFileManager->getOneBy('email', $email);
    }

    public function getByLogin(string $login)
    {
        return $this->jsonFileManager->getOneBy('login', $login);
    }

    public function getAll()
    {
        return $this->jsonFileManager->getAll();
    }

}