<?php

namespace App\Service;

use App\Entity\Notification;
use App\Entity\User;
use App\Interfaces\Notifications;

class SmsService implements Notifications
{
    private $sender;

    public function __construct(SmsFileManagerService $sender)
    {
        $this->sender = $sender;
    }

    public function send(Notification $notification, User $user)
    {
        $this->sender->send($notification, $user);
    }

}