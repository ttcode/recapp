<?php

namespace App\Service;

use App\Entity\Notification;
use App\Entity\User;
use App\Interfaces\Notifications;

class NotificationService implements Notifications
{
    private $channels = [];
    private $userChannels = [];
    private $emailChannelPresent = false;

    public function __construct(EmailService $emailService, SmsService $smsService)
    {
        $this->channels['email'] = $emailService;
        $this->channels['sms'] = $smsService;
    }

    public function send(Notification $notification, User $user)
    {
        $this->prepareUserChannels($user);
        foreach ($this->userChannels as $channel) {
            $trimmedChannelName = trim($channel);
            if (isset($this->channels[$trimmedChannelName])) {
                if ($trimmedChannelName === 'email') {
                    $this->emailChannelPresent = true;
                }
                $this->channels[$trimmedChannelName]->send($notification, $user);
            }
        }

        if (!$this->emailChannelPresent)
        {
            $user->setContactChannels(['email']);
            $this->channels['email']->send($notification, $user);
        }
    }

    private function prepareUserChannels(User $user)
    {
        $this->userChannels = explode(',', $user->getContactChannels());
    }
}