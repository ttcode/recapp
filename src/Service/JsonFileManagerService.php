<?php

namespace App\Service;

class JsonFileManagerService
{
    protected $fileDir = __DIR__ . '/../Data/db.json';

    public function putContent($content)
    {
        $currentContent = json_decode(file_get_contents($this->fileDir));
        $currentContent[] = $content;
        file_put_contents($this->fileDir, json_encode($currentContent));
    }

    public function getOneBy(string $property, string $value)
    {
        $currentContent = json_decode(file_get_contents($this->fileDir));
        foreach ($currentContent as $item) {
            if ($value === $item->{$property}) {
                return $item;
            }
        }
        return null;
    }

    public function getAll()
    {
        return json_decode(file_get_contents($this->fileDir));
    }

}