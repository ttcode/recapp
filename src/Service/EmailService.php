<?php

namespace App\Service;

use App\Entity\Notification;
use App\Entity\User;
use App\Interfaces\Notifications;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class EmailService implements Notifications
{
    private $sender;
    private $params;

    public function __construct(MailerInterface $sender, ContainerBagInterface $params)
    {
        $this->sender = $sender;
        $this->params = $params;
    }

    public function send(Notification $notification, User $user)
    {
        $email = (new Email())->from($this->params->get('EMAIL_LOGIN'))->to($user->getEmail())->subject('Notification alert')->text($notification->getContent());
        $this->sender->send($email);
    }

}