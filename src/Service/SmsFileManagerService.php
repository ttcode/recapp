<?php

namespace App\Service;

use App\Entity\Notification;
use App\Entity\User;

class SmsFileManagerService extends JsonFileManagerService
{
    protected $fileDir = __DIR__ . '/../Data/notifications.json';

    public function send(Notification $notification, User $user)
    {
        $content = [
            'message' => $notification,
            'user' => $user
        ];
        $this->putContent($content);
    }
}