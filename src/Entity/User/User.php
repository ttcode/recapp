<?php

namespace App\Entity;

final class User implements \JsonSerializable {

    private $login;
    private $email;
    private $contactChannels;

    public function __construct(string $login, string $email, ?string $contactChannels)
    {
        $this->login = $login;
        $this->email = $email;
        $this->contactChannels = $contactChannels;
    }

    public function jsonSerialize()
    {
        $vars = get_object_vars($this);
        return $vars;
    }


    /**
     * Get the value of login
     */ 
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set the value of login
     *
     * @return  self
     */ 
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of contactChannels
     */ 
    public function getContactChannels()
    {
        return $this->contactChannels;
    }

    /**
     * Set the value of contactChannels
     *
     * @return  self
     */ 
    public function setContactChannels($contactChannels)
    {
        $this->contactChannels = $contactChannels;

        return $this;
    }
}