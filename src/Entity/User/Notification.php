<?php

namespace App\Entity;

class Notification implements \JsonSerializable
{
    private $content;
    private $sendDate;

    public function __construct(string $content = '')
    {
        $this->content = $content;
        $this->sendDate = date('Y-m-d H:i:s');
    }

    public function jsonSerialize()
    {
        $vars = get_object_vars($this);
        return $vars;
    }


    /**
     * Get the value of content
     */ 
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set the value of content
     *
     * @return  self
     */ 
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get the value of sendDate
     */ 
    public function getSendDate()
    {
        return $this->sendDate;
    }

    /**
     * Set the value of sendDate
     *
     * @return  self
     */ 
    public function setSendDate($sendDate)
    {
        $this->sendDate = $sendDate;

        return $this;
    }
}