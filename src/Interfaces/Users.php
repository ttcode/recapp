<?php

namespace App\Interfaces;

use App\Entity\User;

interface Users 
{

    public function add(User $user);
    public function getByLogin(string $login);
    public function getByEmail(string $email);

}