<?php

namespace App\Interfaces;

use App\Entity\Notification;
use App\Entity\User;

interface Notifications 
{
    public function send(Notification $notification, User $user);
}