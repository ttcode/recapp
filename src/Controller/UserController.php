<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\UserService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserController
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function getUser(Request $request): JsonResponse
    {
        $user = $this->userService->getByLogin($request->get('login'));

        if (!$user) {
            throw new NotFoundHttpException('user not found');
        }

        return new JsonResponse($user);
    }

    public function getUsers(): JsonResponse
    {
        $users = $this->userService->getAll();
        return new JsonResponse($users);
    }

    public function createUser(Request $request): JsonResponse
    {
        $content = json_decode($request->getContent());
        $user = new User($content->login, $content->email, $content->contactChannels);
        $this->fileUsers->add($user);
        return new JsonResponse('', 201);
    }
}
