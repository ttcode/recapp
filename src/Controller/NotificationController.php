<?php

namespace App\Controller;

use App\Entity\Notification;
use App\Entity\User;
use App\Service\NotificationService;
use App\Service\UserService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class NotificationController 
{
    private $notificationService;
    private $userService;

    public function __construct(NotificationService $notificationService, UserService $userService)
    {
        $this->notificationService = $notificationService;
        $this->userService = $userService;
    }

    public function createNotification(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent());
        $notification = new Notification($data->message);
        $userData = $this->userService->getByLogin($data->login);
        $user = new User($userData->login, $userData->email, $userData->contactChannels);
        $this->notificationService->send($notification, $user);
        return new JsonResponse('', 201);
    }
}