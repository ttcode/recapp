<?php

namespace App\Tests;

use App\Service\JsonFileManagerService;
use PHPUnit\Framework\TestCase;
use stdClass;

class JsonFileManagerServiceTest extends TestCase
{
    public function testGetOneBy()
    {
        $expectedUser = new stdClass();
        $expectedUser->login = 'jan.kowalski';
        $expectedUser->email = 'jan.kowalski@test.pl';
        $expectedUser->contactChannels = 'email';

        $jsonFileManager = new JsonFileManagerService();
        $result = $jsonFileManager->getOneBy('login', 'jan.kowalski');

        $this->assertEquals($expectedUser, $result);
    }

    public function testGetAll()
    {
        $jsonFileManager = new JsonFileManagerService();
        $result = $jsonFileManager->getAll();
        $this->assertIsArray($result);
    }
}